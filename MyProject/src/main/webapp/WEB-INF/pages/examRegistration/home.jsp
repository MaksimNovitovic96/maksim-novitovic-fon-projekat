<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<style>
table {
	border-spacing: 10px;
	border-collapse: collapse;
}

table, th, td {
	border: 1px solid black;
}

.link-style {
	padding: 10px;
	font-size: 20px;
	color: #555;
	text-align: center;
	width: 100%;
	font-size: 20px;
}

.details-style {
	text-decoration: none;
	font-size: 18px;
	color: #555;
	text-align: center;
}

.wrapper {
	margin-right: auto;
	margin-left: auto;
	max-width: 960px;
	padding-right: 10px;
	padding-left: 10px;
	background-color: #fff;
}

body {
	background-color: #2c3e50;
	margin: 0px;
}
</style>
<meta charset="ISO-8859-1">
<title>Exam registration home page</title>
</head>
<body>
	<jsp:include page="/WEB-INF/pages/index.jsp"></jsp:include>
	<div class="wrapper">
		<div class="details-style">
			<div class="link-style">
				<c:url value="/examRegistration/add" var="examRegistrationAdd"></c:url>
				<a class="link-style" href="<c:out value="${examRegistrationAdd}"/>">Add exam
					registration</a>
			</div>

			<div class="link-style">
				<c:url value="/examRegistration/all" var="examRegistrationAll"></c:url>
				<a class="link-style" href="<c:out value="${examRegistrationAll}"/>">All exam
					registration</a>
			</div>
		</div>
	</div>

</body>
</html>