<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Add exam Registration</title>
<style>
table {
	border-spacing: 10px;
	border-collapse: collapse;
}

table, th, td {
	border: 1px solid black;
}

.link-style {
	padding: 10px;
	font-size: 20px;
	color: #555;
	text-align: center;
	width: 100%;
	font-size: 20px;
}

.details-style {
	text-decoration: none;
	font-size: 18px;
	color: #555;
	text-align: center;
}

.wrapper {
	margin-right: auto;
	margin-left: auto;
	max-width: 960px;
	padding-right: 10px;
	padding-left: 10px;
	background-color: #fff;
}

body {
	background-color: #2c3e50;
	margin: 0px;
}
</style>
</head>
<body>
<jsp:include page="/WEB-INF/pages/index.jsp"></jsp:include>
	<div class="wrapper">
		<div class="details-style">
<form:form action="/maksim.novitovic.fon/examRegistration/save" method="post"
		modelAttribute="examRegistrationDto">
		
		Exam:
		<form:label path="examDto" />
		<form:select path="examDto">
			<form:options items="${exams}" itemValue="id" itemLabel="subjectDto.name" />
		</form:select>
		<form:errors path="examDto" cssClass="error" />
		<p />
		
		Student:
		<form:label path="studentDto" />
		<form:select path="studentDto">
			<form:options items="${students}" itemValue="id" itemLabel="fullname" />
		</form:select>
		<form:errors path="studentDto" cssClass="error" />
		<p />
		

		<button id="save">Save</button>
	</form:form>
</div></div>
</body>
</html>