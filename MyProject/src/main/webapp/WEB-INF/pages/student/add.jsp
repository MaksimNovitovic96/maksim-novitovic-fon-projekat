<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Add:student</title>
<style>
table {
	border-spacing: 10px;
	border-collapse: collapse;
}

table, th, td {
	border: 1px solid black;
}

.link-style {
	padding: 10px;
	font-size: 20px;
	color: #555;
	text-align: center;
	width: 100%;
	font-size: 20px;
}

.details-style {
	text-decoration: none;
	font-size: 18px;
	color: #555;
	text-align: center;
}

.wrapper {
	margin-right: auto;
	margin-left: auto;
	max-width: 960px;
	padding-right: 10px;
	padding-left: 10px;
	background-color: #fff;
}

body {
	background-color: #2c3e50;
	margin: 0px;
}
</style>
</head>
<body>
<jsp:include page="/WEB-INF/pages/index.jsp"></jsp:include>
	<div class="wrapper">
		<div class="details-style">

	<form:form action="/maksim.novitovic.fon/student/save" method="post"
		modelAttribute="studentDto">
		IndexNumber:<form:input type="text" path="indexNumber"
			id="indexNumberId" />
		<br />
		<form:errors path="indexNumber" cssClass="error" />
		<p />
		FirstName:<form:input type="text" path="firstName" id="firstnameId" />
		<br />
		<form:errors path="firstName" cssClass="error" />
		<p />
		LastName:<form:input type="text" path="lastName" id="lastnameId" />
		<br />
		<form:errors path="lastName" cssClass="error" />
		<p />
		Email:<form:input type="text" path="email" id="emailId" />
		<br />
		<form:errors path="email" cssClass="error" />
		<p />
		Address:<form:input type="text" path="address" id="addressId" />
		<br />
		<form:errors path="address" cssClass="error" />
		City:
		<form:label path="cityDto" />
		<form:select path="cityDto">
			<form:option value="" label="Choose City"></form:option>
			<form:options items="${cities}" itemValue="id" itemLabel="name" />
		</form:select>
		<form:errors path="cityDto" cssClass="error" />

		<p />
		Phone:<form:input type="text" path="phone" id="phoneId" />
		<br />
		<form:errors path="phone" cssClass="error" />
		<p />
		Current Year of Study:<form:input type="text"
			path="currentYearOfStudy" id="currentYearOfStudyId" />
		<br />
		<form:errors path="currentYearOfStudy" cssClass="error" />

		<button id="save">Save</button>
	</form:form>
	</div></div>
</body>
</html>