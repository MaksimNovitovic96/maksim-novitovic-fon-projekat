<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>All students</title>
<style>
table {
	border-spacing: 10px;
	border-collapse: collapse;
}

table, th, td {
	border: 1px solid black;
}

.link-style {
	padding: 10px;
	font-size: 20px;
	color: #555;
	text-align: center;
	width: 100%;
	font-size: 20px;
}

.details-style {
	text-decoration: none;
	font-size: 18px;
	color: #555;
	text-align: center;
}

.wrapper {
	margin-right: auto;
	margin-left: auto;
	max-width: 960px;
	padding-right: 10px;
	padding-left: 10px;
	background-color: #fff;
}

body {
	background-color: #2c3e50;
	margin: 0px;
}
</style>
</head>
<body>
	<jsp:include page="/WEB-INF/pages/index.jsp"></jsp:include>
	<div class="wrapper">
		<div class="details-style">

			<table>
				<tbody>
					<tr>
						<th>IndexNumber</th>
						<th>FirstName</th>
						<th>LastName</th>
						<th>Email</th>
						<th>Address</th>
						<th>City</th>
						<th>Phone</th>
						<th>Current year</th>
						<th>Details</th>
						<th>Edit</th>
						<th>Remove</th>
					</tr>
					<c:forEach items="${students}" var="student">

						<c:url value="/student/details" var="studentDetails">
							<c:param name="id" value="${student.id}"></c:param>
						</c:url>

						<c:url value="/student/edit" var="studentEdit">
							<c:param name="id" value="${student.id}"></c:param>
						</c:url>

						<c:url value="/student/remove" var="studentRemove">
							<c:param name="id" value="${student.id}"></c:param>
						</c:url>
						<tr>
							<td>${student.indexNumber}</td>
							<td>${student.firstName}</td>
							<td>${student.lastName}</td>
							<td>${student.email}</td>
							<td>${student.address}</td>
							<td>${student.cityDto.name}</td>
							<td>${student.phone}</td>
							<td>${student.currentYearOfStudy}</td>
							<td><a href="${studentDetails}">Details</a></td>
							<td><a href="${studentEdit}">Edit</a></td>
							<td><a href="${studentRemove}">Remove</a></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
</body>
</html>