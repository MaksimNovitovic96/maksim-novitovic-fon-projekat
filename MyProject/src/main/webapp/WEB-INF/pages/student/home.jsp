<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<style>
table {
	border-spacing: 10px;
	border-collapse: collapse;
}

table, th, td {
	border: 1px solid black;
}

.link-style {
	padding: 10px;
	font-size: 20px;
	color: #555;
	text-align: center;
	width: 100%;
	font-size: 20px;
}

.details-style {
	text-decoration: none;
	font-size: 18px;
	color: #555;
	text-align: center;
}

.wrapper {
	margin-right: auto;
	margin-left: auto;
	max-width: 960px;
	padding-right: 10px;
	padding-left: 10px;
	background-color: #fff;
}

body {
	background-color: #2c3e50;
	margin: 0px;
}
</style>
</head>
<body>
<jsp:include page="/WEB-INF/pages/index.jsp"></jsp:include>
	<div class="wrapper">
		<div class="details-style">

<div class="link-style">
		<c:url value="/student/add" var="studentAdd"></c:url>
		<a class="link-style" href="<c:out value="${studentAdd}"/>">Add student</a>
	</div>

	<div class="link-style">
		<c:url value="/student/all" var="studentAll"></c:url>
		<a class="link-style" href="<c:out value="${studentAll}"/>">All students</a>
	</div>
</div></div>
</body>
</html>