<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Add exam:</title>
<style>
table {
	border-spacing: 10px;
	border-collapse: collapse;
}

table, th, td {
	border: 1px solid black;
}

.link-style {
	padding: 10px;
	font-size: 20px;
	color: #555;
	text-align: center;
	width: 100%;
	font-size: 20px;
}

.details-style {
	text-decoration: none;
	font-size: 18px;
	color: #555;
	text-align: center;
}

.wrapper {
	margin-right: auto;
	margin-left: auto;
	max-width: 960px;
	padding-right: 10px;
	padding-left: 10px;
	background-color: #fff;
}

body {
	background-color: #2c3e50;
	margin: 0px;
}
</style>
</head>
<body>
<jsp:include page="/WEB-INF/pages/index.jsp"></jsp:include>
	<div class="wrapper">
		<div class="details-style">

	<form:form action="/maksim.novitovic.fon/exam/save" method="post"
		modelAttribute="examDto">
		
		Subject:
		<form:label path="subjectDto" />
		<form:select path="subjectDto">
			<form:options items="${subjects}" itemValue="id" itemLabel="name" />
		</form:select>
		<form:errors path="subjectDto" cssClass="error" />
		<p />
		
		Professor:
		<form:label path="professorDto" />
		<form:select path="professorDto">
			<form:options items="${professors}" itemValue="id" itemLabel="fullname" />
		</form:select>
		<form:errors path="professorDto" cssClass="error" />
		<p />
		Date:<form:input type="date" path="date"
			id="dateId" />
		<br />
		<form:errors path="date" cssClass="error" />
		<p />


		<button id="save">Save</button>
	</form:form>
</div></div>
</body>
</html>