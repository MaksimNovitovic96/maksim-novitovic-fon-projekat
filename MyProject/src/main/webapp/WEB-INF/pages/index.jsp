<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<style type="text/css">
.link-style {
	text-decoration: none;
	font-size: 20px;
	color: #555;
	text-align: center;
	width: 100%;
}

.wrapper {
	margin-right: auto;
	margin-left: auto;
	max-width: 960px;
	padding-right: 10px;
	padding-left: 10px;
	background-color: #fff;
	min-height: 100px;
}

body {
	background-color: #2c3e50;
	margin: 0px;
}
.align-links {
	display: flex;
	justify-content: space-between;
}
.link-div {
	
	transition: background-color 0.2s;
	width: 100%;
	display: flex;
	justify-content: center;
	align-items: center;
}
.link-div:hover {
	background-color: #bdc3c7;
}
</style>
<meta charset="ISO-8859-1">
<title>index jsp.page</title>
</head>

<body>

	<div class="wrapper">
		<div class="align-links">
		<c:url value="/student/home" var="studentHome"></c:url>
			<a class="link-style" href="<c:out value="${studentHome}"/>">
			
				<div class="link-div">
					
					Student
				</div>
			</a>
			<div class="link-div">
				<c:url value="/professor/home" var="professorHome"></c:url>
				<a class="link-style" href="<c:out value="${professorHome}"/>">Professors</a>
			</div>
			<div class="link-div">
				<c:url value="/subject/home" var="subjectHome"></c:url>
				<a class="link-style" href="<c:out value="${subjectHome}"/>">Subject</a>
			</div>

			<div class="link-div">
				<c:url value="/exam/home" var="examHome"></c:url>
				<a class="link-style" href="<c:out value="${examHome}"/>">Exam</a>
			</div>

			<div class="link-div">
				<c:url value="/examRegistration/home" var="examRegistrationHome"></c:url>
				<a class="link-style"
					href="<c:out value="${examRegistrationHome}"/>">Exam Registration</a>
			</div>
		</div>
	</div>
</body>
</html>