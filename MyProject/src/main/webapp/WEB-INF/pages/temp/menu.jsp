<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<div>
		<c:url value="/student/add" var="studentAdd"></c:url>
		<a href="<c:out value="${studentAdd}"/>">Add student</a>
	</div>

	<div>
		<c:url value="/student/all" var="studentAll"></c:url>
		<a href="<c:out value="${studentAll}"/>">All students</a>
	</div>

	<div>
		<c:url value="/professor/add" var="professorAdd"></c:url>
		<a href="<c:out value="${professorAdd}"/>">Add professor</a>
	</div>

	<div>
		<c:url value="/professor/all" var="professorAll"></c:url>
		<a href="<c:out value="${professorAll}"/>">All professor</a>
	</div>
	<div>
		<c:url value="/subject/add" var="subjectAdd"></c:url>
		<a href="<c:out value="${subjectAdd}"/>">Add subject</a>
	</div>

	<div>
		<c:url value="/subject/all" var="subjectAll"></c:url>
		<a href="<c:out value="${subjectAll}"/>">All subject</a>
	</div>
</body>
</html>