<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Login</title>
<style type="text/css">
.error {
	color: red;
}
</style>
</head>
<body>
	<form:form action="/maksim.novitovic.fon/authentication/login" method="post"
		modelAttribute="userDto">
		Username:<form:input type="text" path="username" id="usernameId" />
		<br />
		<form:errors path="username" cssClass="error" />
		<p />
		Password::<form:input type="text" path="password" id="passwordId" />
		<br />
		<form:errors path="password" cssClass="error" />
		<p />
		<button id="Login">Login</button>
	</form:form>

</body>
</html>