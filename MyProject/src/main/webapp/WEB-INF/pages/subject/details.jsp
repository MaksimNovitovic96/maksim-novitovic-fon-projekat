<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>details subject</title>
<style>
table {
	border-spacing: 10px;
	border-collapse: collapse;
}

table, th, td {
	border: 1px solid black;
}

.link-style {
	padding: 10px;
	font-size: 20px;
	color: #555;
	text-align: center;
	width: 100%;
	font-size: 20px;
}

.details-style {
	text-decoration: none;
	font-size: 18px;
	color: #555;
	text-align: center;
}

.wrapper {
	margin-right: auto;
	margin-left: auto;
	max-width: 960px;
	padding-right: 10px;
	padding-left: 10px;
	background-color: #fff;
}

body {
	background-color: #2c3e50;
	margin: 0px;
}
</style>
</head>
<body>
	<jsp:include page="/WEB-INF/pages/index.jsp"></jsp:include>
	<div class="wrapper">
		<div class="details-style">
			<table>
				<tbody>
					<tr>
						<th>Name</th>
						<th>Description</th>
						<th>Year Of Study</th>
						<th>Semester</th>

					</tr>
					<tr>
						<td>${subjectDto.name}</td>
						<td>${subjectDto.description}</td>
						<td>${subjectDto.yearOfStudy}</td>
						<td>${subjectDto.semester}</td>

					</tr>
				</tbody>
			</table>
		</div>
	</div>
</body>
</html>