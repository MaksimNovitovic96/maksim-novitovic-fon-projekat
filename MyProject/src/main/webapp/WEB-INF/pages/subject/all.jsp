<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>All subject</title>
<style>
table {
	border-spacing: 10px;
	border-collapse: collapse;
}

table, th, td {
	border: 1px solid black;
}

.link-style {
	padding: 10px;
	font-size: 20px;
	color: #555;
	text-align: center;
	width: 100%;
	font-size: 20px;
}

.details-style {
	text-decoration: none;
	font-size: 18px;
	color: #555;
	text-align: center;
}

.wrapper {
	margin-right: auto;
	margin-left: auto;
	max-width: 960px;
	padding-right: 10px;
	padding-left: 10px;
	background-color: #fff;
}

body {
	background-color: #2c3e50;
	margin: 0px;
}
</style>
</head>
<body>
	<jsp:include page="/WEB-INF/pages/index.jsp"></jsp:include>
	<div class="wrapper">
		<div class="details-style">
			<table>
				<tbody>
					<tr>
						<th>Name</th>
						<th>Description</th>
						<th>Year Of Study</th>
						<th>Semester</th>
						<th>Details</th>
						<th>Edit</th>
						<th>Remove</th>
					</tr>
					<c:forEach items="${subjects}" var="subject">
						<c:url value="/subject/details" var="subjectDetails">
							<c:param name="id" value="${subject.id}"></c:param>
						</c:url>
						<c:url value="/subject/edit" var="subjectEdit">
							<c:param name="id" value="${subject.id}"></c:param>
						</c:url>

						<c:url value="remove" var="subjectRemove">
							<c:param name="id" value="${subject.id}"></c:param>
						</c:url>
						<tr>
							<td>${subject.name}</td>
							<td>${subject.description}</td>
							<td>${subject.yearOfStudy}</td>
							<td>${subject.semester}</td>
							<td><a href="${subjectDetails}">Details</a></td>
							<td><a href="${subjectEdit}">Edit</a></td>
							<td><a href="${subjectRemove}">Remove</a></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
</body>
</html>