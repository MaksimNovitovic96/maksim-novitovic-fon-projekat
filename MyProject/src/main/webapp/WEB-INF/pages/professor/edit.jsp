<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<style>
table {
	border-spacing: 10px;
	border-collapse: collapse;
}

table, th, td {
	border: 1px solid black;
}

.link-style {
	padding: 10px;
	font-size: 20px;
	color: #555;
	text-align: center;
	width: 100%;
	font-size: 20px;
}

.details-style {
	text-decoration: none;
	font-size: 18px;
	color: #555;
	text-align: center;
}

.wrapper {
	margin-right: auto;
	margin-left: auto;
	max-width: 960px;
	padding-right: 10px;
	padding-left: 10px;
	background-color: #fff;
}

body {
	background-color: #2c3e50;
	margin: 0px;
}
</style>
<meta charset="ISO-8859-1">
<title>Edit Professor</title>
</head>
<body>
	<jsp:include page="/WEB-INF/pages/index.jsp"></jsp:include>
	<div class="wrapper">
<div class="details-style">

	<form:form action="/maksim.novitovic.fon/professor/update"
		method="post" modelAttribute="professorDto">
		<form:hidden path="id" id="id" />
		First Name:<form:input type="text" path="firstname" id="firstnameId" />
		<br />
		<form:errors path="firstname" cssClass="error" />
		<p />
		Last Name:<form:input type="text" path="lastname" id="lastnameId" />
		<br />
		<form:errors path="lastname" cssClass="error" />
		<p />
		Email:<form:input type="text" path="email" id="emailId" />
		<br />
		<form:errors path="email" cssClass="error" />
		<p />
		Address:<form:input type="text" path="address" id="addressId" />
		<br />
		<form:errors path="address" cssClass="error" />
		<p />
		City:
		<form:label path="cityDto" />
		<form:select path="cityDto">
			<form:option value="" label="Choose City"></form:option>
			<form:options items="${cities}" itemValue="id" itemLabel="name" />
		</form:select>
		<form:errors path="cityDto" cssClass="error" />

		<p />
		Phone:<form:input type="text" path="phone" id="phoneId" />
		<br />
		<form:errors path="phone" cssClass="error" />
		<p />
		Reelection date:<form:input type="date" path="reelectionDate"
			id="reelectionDateId" />
		<br />
		<form:errors path="reelectionDate" cssClass="error" />
		<p />
		Title:
		<form:label path="titleDto" />
		<form:select path="titleDto">
			<!--  	<form:option value="" label="Choose City"></form:option>-->
			<form:options items="${titles}" itemValue="id" itemLabel="type" />
		</form:select>
		<form:errors path="titleDto" cssClass="error" />
		<br />
		<button id="save">Save</button>
	</form:form>
</div></div>
</body>
</html>