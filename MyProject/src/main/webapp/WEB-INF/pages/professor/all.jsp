<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<style>
table {
	border-spacing: 10px;
	border-collapse: collapse;
}

table, th, td {
	border: 1px solid black;
}

.link-style {
	padding: 10px;
	font-size: 20px;
	color: #555;
	text-align: center;
	width: 100%;
	font-size: 20px;
}

.details-style {
	text-decoration: none;
	font-size: 18px;
	color: #555;
	text-align: center;
}

.wrapper {
	margin-right: auto;
	margin-left: auto;
	max-width: 960px;
	padding-right: 10px;
	padding-left: 10px;
	background-color: #fff;
}

body {
	background-color: #2c3e50;
	margin: 0px;
}
</style>
<meta charset="ISO-8859-1">
<title>All Professors</title>
</head>
<body>

	<jsp:include page="/WEB-INF/pages/index.jsp"></jsp:include>
	<div class="wrapper">
		<div class="details-style">
			<table>
				<tbody>
					<tr>
						<th>First name</th>
						<th>Last name</th>
						<th>Email</th>
						<th>Address</th>
						<th>City</th>
						<th>Phone</th>
						<th>Reelection date</th>
						<th>Title</th>
						<th>Details</th>
						<th>Edit</th>
						<th>Remove</th>
					</tr>

					<c:forEach items="${professors}" var="professor">

						<c:url value="/professor/details" var="professorDetails">
							<c:param name="id" value="${professor.id}"></c:param>
						</c:url>

						<c:url value="/professor/edit" var="professorEdit">
							<c:param name="id" value="${professor.id}"></c:param>
						</c:url>

						<c:url value="/professor/remove" var="professorRemove">
							<c:param name="id" value="${professor.id}"></c:param>
						</c:url>
						<tr>
							<td>${professor.firstname}</td>
							<td>${professor.lastname}</td>
							<td>${professor.email}</td>
							<td>${professor.address}</td>
							<td>${professor.cityDto.name}</td>
							<td>${professor.phone}</td>
							<td>${professor.reelectionDate}</td>
							<td>${professor.titleDto.type}</td>
							<td><a href="${professorDetails}">Details</a></td>
							<td><a href="${professorEdit}">Edit</a></td>
							<td><a href="${professorRemove}">Remove</a></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
</body>
</html>