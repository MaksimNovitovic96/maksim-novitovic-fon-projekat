<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<style>
table {
	border-spacing: 10px;
	border-collapse: collapse;
}

table, th, td {
	border: 1px solid black;
}

.details-style {
	text-decoration: none;
	font-size: 18px;
	color: #555;
	text-align: center;
}

.wrapper {
	margin-right: auto;
	margin-left: auto;
	max-width: 960px;
	padding-right: 10px;
	padding-left: 10px;
	background-color: #fff;
}

body {
	background-color: #2c3e50;
	margin: 0px;
}
</style>
<meta charset="ISO-8859-1">
<title>Details</title>
<jsp:include page="/WEB-INF/pages/index.jsp"></jsp:include>
</head>
<body>
	<div class="wrapper">
		<div class="details-style">
			<table>

				<tbody>

					<tr>
						<th>First name</th>

						<th>Last name</th>
						<th>Email</th>
						<th>Address</th>
						<th>City</th>
						<th>Phone</th>
						<th>Reelection date</th>
						<th>Title</th>

					</tr>

					<tr>
						<td>${professorDto.firstname}</td>
						<td>${professorDto.lastname}</td>
						<td>${professorDto.email}</td>
						<td>${professorDto.address}</td>
						<td>${professorDto.cityDto.name}</td>
						<td>${professorDto.phone}</td>
						<td>${professorDto.reelectionDate}</td>
						<td>${professorDto.titleDto.type}</td>

					</tr>

				</tbody>
			</table>
		</div>
	</div>
</body>
</html>