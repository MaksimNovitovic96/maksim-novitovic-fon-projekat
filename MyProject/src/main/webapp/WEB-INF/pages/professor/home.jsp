<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<style>
table {
	border-spacing: 10px;
	border-collapse: collapse;
}

table, th, td {
	border: 1px solid black;
}

.link-style {
	padding: 10px;
	font-size: 20px;
	color: #555;
	text-align: center;
	width: 100%;
	font-size: 20px;
}

.details-style {
	text-decoration: none;
	font-size: 18px;
	color: #555;
	text-align: center;
}

.wrapper {
	margin-right: auto;
	margin-left: auto;
	max-width: 960px;
	padding-right: 10px;
	padding-left: 10px;
	background-color: #fff;
}

body {
	background-color: #2c3e50;
	margin: 0px;
}
</style>
<meta charset="ISO-8859-1">
<title>Professor home page</title>
<jsp:include page="/WEB-INF/pages/index.jsp"></jsp:include>
</head>
<body>
	<div class="wrapper">
		<div class="details-style">
			<div class="link-style">
				<c:url value="/professor/add" var="professorAdd"></c:url>
				<a class="link-style"href="<c:out value="${professorAdd}"/>">Add professors</a>
			</div>

			<div class="link-style">
				<c:url value="/professor/all" var="professorAll"></c:url>
				<a class="link-style" href="<c:out value="${professorAll}"/>">All professors</a>
			</div>
		</div>
	</div>
</body>
</html>