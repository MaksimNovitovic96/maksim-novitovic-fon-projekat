package maksim.novitovic.fon.myproject.formatter;

import java.text.ParseException;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;

import maksim.novitovic.fon.myproject.dto.StudentDto;
import maksim.novitovic.fon.myproject.service.StudentService;

@Component
public class StudentDtoFormatter implements Formatter<StudentDto> {
	private final StudentService studentService;

	@Autowired
	public StudentDtoFormatter(StudentService studentService) {
		this.studentService = studentService;
	}

	@Override
	public String print(StudentDto object, Locale locale) {

		return object.getId().toString();
	}

	@Override
	public StudentDto parse(String text, Locale locale) throws ParseException {
		Long id = Long.parseLong(text);
		StudentDto studentDto = studentService.findById(id);
		return studentDto;
	}
}
