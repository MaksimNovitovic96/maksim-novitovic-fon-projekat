package maksim.novitovic.fon.myproject.converter;

import org.springframework.stereotype.Component;

import maksim.novitovic.fon.myproject.dto.TitleDto;
import maksim.novitovic.fon.myproject.entity.TitleEntity;

@Component
public class TitleConverter {

	public TitleDto entityToDto(TitleEntity titleEntity) {
		if(titleEntity==null) return null;
		return new TitleDto(titleEntity.getId(), titleEntity.getRole());
	}

	public TitleEntity dtoToEntity(TitleDto titleDto) {
		if(titleDto==null) return null;
		return new TitleEntity(titleDto.getId(), titleDto.getType());
	}
}
