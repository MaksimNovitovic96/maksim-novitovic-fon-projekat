package maksim.novitovic.fon.myproject.service;

import java.util.List;

import maksim.novitovic.fon.myproject.dto.StudentDto;

public interface StudentService {

	public void save(StudentDto studentDto);
	public void update(Long id,StudentDto studentDto);
	public void delete(Long id);
	public StudentDto findById(Long id);
	public List<StudentDto> getAll();
}
