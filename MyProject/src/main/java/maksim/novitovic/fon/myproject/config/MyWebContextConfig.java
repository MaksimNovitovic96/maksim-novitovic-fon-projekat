package maksim.novitovic.fon.myproject.config;


import java.text.SimpleDateFormat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import maksim.novitovic.fon.myproject.formatter.*;
import maksim.novitovic.fon.myproject.formatter.ProfessorDtoFormatter;
import maksim.novitovic.fon.myproject.formatter.ReelectionDateFormatter;
import maksim.novitovic.fon.myproject.service.CityService;
import maksim.novitovic.fon.myproject.service.ExamService;
import maksim.novitovic.fon.myproject.service.ProfessorService;
import maksim.novitovic.fon.myproject.service.StudentService;
import maksim.novitovic.fon.myproject.service.SubjectService;
import maksim.novitovic.fon.myproject.service.TitleService;


@EnableWebMvc
@ComponentScan(basePackages = { "maksim.novitovic.fon.myproject.controller",
		"maksim.novitovic.fon.myproject.formatter" })
@Configuration
public class MyWebContextConfig implements WebMvcConfigurer {

	private CityService cityService;
	private TitleService titleService;
	private SubjectService subjectService;
	private ProfessorService professorService;
	private ExamService examService;
	private StudentService studentService;

	@Autowired
	public MyWebContextConfig(CityService cityService, TitleService titleService, SubjectService subjectService,
			ProfessorService professorService, ExamService examService, StudentService studentService) {
		this.cityService = cityService;
		this.titleService = titleService;
		this.subjectService = subjectService;
		this.professorService = professorService;
		this.examService = examService;
		this.studentService = studentService;
		System.out.println("=================================================================");
		System.out.println("==================== MyWebContextConfig =========================");
		System.out.println("=================================================================");
	}
	@Bean
	public ViewResolver viewResolver() {
		InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
		viewResolver.setPrefix("/WEB-INF/pages/");
		viewResolver.setSuffix(".jsp");
		return viewResolver;
	}

	@Bean
	public SimpleDateFormat simpleDateFormat() {
		return new SimpleDateFormat("yyyy-MM-dd");
	}

	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
		registry.addViewController("/").setViewName("index");
	}

	@Override
	public void addFormatters(FormatterRegistry registry) {
		registry.addFormatter(new CityDtoFormatter(cityService));
		registry.addFormatter(new TitleDtoFormatter(titleService));
		registry.addFormatter(new ReelectionDateFormatter(simpleDateFormat()));
		registry.addFormatter(new SubjectDtoFormatter(subjectService));
		registry.addFormatter(new ProfessorDtoFormatter(professorService));
		registry.addFormatter(new ExamDtoFormatter(examService));
		registry.addFormatter(new StudentDtoFormatter(studentService));
	}

}
