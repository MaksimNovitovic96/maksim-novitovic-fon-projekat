package maksim.novitovic.fon.myproject.formatter;

import java.text.ParseException;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;

import maksim.novitovic.fon.myproject.dto.SubjectDto;
import maksim.novitovic.fon.myproject.service.SubjectService;

public class SubjectDtoFormatter implements Formatter<SubjectDto> {
	private final SubjectService subjectService;

	@Autowired
	public SubjectDtoFormatter(SubjectService subjectService) {
		this.subjectService = subjectService;
	}

	@Override
	public String print(SubjectDto subjectDto, Locale locale) {

		return subjectDto.getId().toString();
	}

	@Override
	public SubjectDto parse(String text, Locale locale) throws ParseException {
		Long id = Long.parseLong(text);
		return subjectService.findById(id);
	}

}