package maksim.novitovic.fon.myproject.service;

import java.util.List;

import maksim.novitovic.fon.myproject.dto.ProfessorDto;

public interface ProfessorService {

	public void delete(Long id);

	public ProfessorDto findById(Long id);

	public List<ProfessorDto> getAll();

	public void save(ProfessorDto professorDto);

}
