package maksim.novitovic.fon.myproject.repository.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import maksim.novitovic.fon.myproject.entity.TitleEntity;
@Repository
public interface TitleRepository extends JpaRepository<TitleEntity, Long>{

}
