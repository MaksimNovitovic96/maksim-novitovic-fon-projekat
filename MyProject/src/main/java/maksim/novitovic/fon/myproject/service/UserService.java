package maksim.novitovic.fon.myproject.service;

import java.util.List;

import maksim.novitovic.fon.myproject.dto.UserDto;

public interface UserService {
	public List<UserDto> findUserByNameAndPassword(UserDto userDto);
}
