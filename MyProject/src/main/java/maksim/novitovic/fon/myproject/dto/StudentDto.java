package maksim.novitovic.fon.myproject.dto;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

public class StudentDto implements Serializable {

	private Long id;

	@NotEmpty(message = "You must add index number.")
	@Length(min = 10, message = "Minimum number of characters for IndexNumber is 10")
	private String indexNumber;
	@Length(min = 3, message = "Minimum number of characters for First Name is 3")
	@NotEmpty(message = "You must add firstname.")
	private String firstName;
	@NotEmpty(message = "You must add lastname.")
	@Length(min = 3, message = "Minimum number of characters for Last Name is 3")
	private String lastName;

	@NotEmpty(message = "You must add email.")
	private String email;

	@NotEmpty(message = "You must add address.")
	private String address;

	@NotEmpty(message = "You must add phone number.")
	private String phone;

	@NotNull(message = "You must add current year of study.")
	private Integer currentYearOfStudy;

	private CityDto cityDto;

	public StudentDto() {

	}

	public StudentDto(Long id,
			@NotEmpty(message = "You must add index number.") @Length(min = 10, message = "Minimum number of characters for IndexNumber is 10") String indexNumber,
			@Length(min = 3, message = "Minimum number of characters for First Name is 3") @NotEmpty(message = "You must add firstname.") String firstName,
			@NotEmpty(message = "You must add lastname.") @Length(min = 3, message = "Minimum number of characters for Last Name is 3") String lastName,
			@NotEmpty(message = "You must add email.") String email,
			@NotEmpty(message = "You must add address.") String address,
			@NotEmpty(message = "You must add phone number.") String phone,
			@NotNull(message = "You must add current year of study.") Integer currentYearOfStudy, CityDto cityDto) {
		super();
		this.id = id;
		this.indexNumber = indexNumber;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.address = address;
		this.phone = phone;
		this.currentYearOfStudy = currentYearOfStudy;
		this.cityDto = cityDto;
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIndexNumber() {
		return indexNumber;
	}

	public void setIndexNumber(String indexNumber) {
		this.indexNumber = indexNumber;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Integer getCurrentYearOfStudy() {
		return currentYearOfStudy;
	}

	public void setCurrentYearOfStudy(Integer currentYearOfStudy) {
		this.currentYearOfStudy = currentYearOfStudy;
	}

	public CityDto getCityDto() {
		return cityDto;
	}

	public void setCityDto(CityDto cityDto) {
		this.cityDto = cityDto;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + ((cityDto == null) ? 0 : cityDto.hashCode());
		result = prime * result + ((currentYearOfStudy == null) ? 0 : currentYearOfStudy.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((indexNumber == null) ? 0 : indexNumber.hashCode());
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((phone == null) ? 0 : phone.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StudentDto other = (StudentDto) obj;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (cityDto == null) {
			if (other.cityDto != null)
				return false;
		} else if (!cityDto.equals(other.cityDto))
			return false;
		if (currentYearOfStudy == null) {
			if (other.currentYearOfStudy != null)
				return false;
		} else if (!currentYearOfStudy.equals(other.currentYearOfStudy))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (indexNumber == null) {
			if (other.indexNumber != null)
				return false;
		} else if (!indexNumber.equals(other.indexNumber))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (phone == null) {
			if (other.phone != null)
				return false;
		} else if (!phone.equals(other.phone))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "StudentDto [id=" + id + ", indexNumber=" + indexNumber + ", firstName=" + firstName + ", lastName="
				+ lastName + ", email=" + email + ", address=" + address + ", phone=" + phone + ", currentYearOfStudy="
				+ currentYearOfStudy + ", city=" + cityDto + "]";
	}
	public String getFullname() {
		return firstName + " " + lastName;
	}
}