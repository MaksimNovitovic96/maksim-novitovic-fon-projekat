package maksim.novitovic.fon.myproject.service;

import java.util.List;

import maksim.novitovic.fon.myproject.dto.ExamDto;

public interface ExamService {

	public void save(ExamDto examDto);
	public ExamDto findById(Long id);
	public List<ExamDto> getAll();
}
