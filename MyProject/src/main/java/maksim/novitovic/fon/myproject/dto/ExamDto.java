package maksim.novitovic.fon.myproject.dto;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class ExamDto implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long id;

	@NotNull(message = "Date can not be empty! Please choose date")
	private Date date;
	@NotNull(message = "Please choose professor")
	private ProfessorDto professorDto;
	@NotNull(message = "Please choose subject")
	private SubjectDto subjectDto;

	public ExamDto() {

	}

	public ExamDto(Long id, @NotEmpty(message = "Add date of the exam.") Date date,
			@NotNull(message = "Please choose professor") ProfessorDto professorDto,
			@NotNull(message = "Please choose subject") SubjectDto subjectDto) {
		super();
		this.id = id;
		this.date = date;
		this.professorDto = professorDto;
		this.subjectDto = subjectDto;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public ProfessorDto getProfessorDto() {
		return professorDto;
	}

	public void setProfessorDto(ProfessorDto professorDto) {
		this.professorDto = professorDto;
	}

	public SubjectDto getSubjectDto() {
		return subjectDto;
	}

	public void setSubjectDto(SubjectDto subjectDto) {
		this.subjectDto = subjectDto;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((professorDto == null) ? 0 : professorDto.hashCode());
		result = prime * result + ((subjectDto == null) ? 0 : subjectDto.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ExamDto other = (ExamDto) obj;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (professorDto == null) {
			if (other.professorDto != null)
				return false;
		} else if (!professorDto.equals(other.professorDto))
			return false;
		if (subjectDto == null) {
			if (other.subjectDto != null)
				return false;
		} else if (!subjectDto.equals(other.subjectDto))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ExamDto [id=" + id + ", date=" + date + ", professorDto=" + professorDto + ", subjectDto=" + subjectDto
				+ "]";
	}

}