package maksim.novitovic.fon.myproject.dto;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class ExamRegistrationDto implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long id;

	@NotNull(message = "Please choose exam")
	private ExamDto examDto;
	@NotNull(message = "Please choose student")
	private StudentDto studentDto;

	public ExamRegistrationDto() {
	
	}

	public ExamRegistrationDto(Long id, @NotNull(message = "Please choose exam") ExamDto examDto,
			@NotNull(message = "Please choose student") StudentDto studentDto) {
		super();
		this.id = id;
		this.examDto = examDto;
		this.studentDto = studentDto;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ExamDto getExamDto() {
		return examDto;
	}

	public void setExamDto(ExamDto examDto) {
		this.examDto = examDto;
	}

	public StudentDto getStudentDto() {
		return studentDto;
	}

	public void setStudentDto(StudentDto studentDto) {
		this.studentDto = studentDto;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((examDto == null) ? 0 : examDto.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((studentDto == null) ? 0 : studentDto.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ExamRegistrationDto other = (ExamRegistrationDto) obj;
		if (examDto == null) {
			if (other.examDto != null)
				return false;
		} else if (!examDto.equals(other.examDto))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (studentDto == null) {
			if (other.studentDto != null)
				return false;
		} else if (!studentDto.equals(other.studentDto))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ExamRegistrationDto [id=" + id + ", examDto=" + examDto + ", studentDto=" + studentDto + "]";
	}

}
