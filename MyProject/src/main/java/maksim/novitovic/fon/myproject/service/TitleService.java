package maksim.novitovic.fon.myproject.service;

import java.util.List;

import maksim.novitovic.fon.myproject.dto.TitleDto;

public interface TitleService {
	public void save(TitleDto titleDto);
	public TitleDto findById(Long id);
	public List<TitleDto> getAll();
}
