package maksim.novitovic.fon.myproject.service;
import java.util.List;
import maksim.novitovic.fon.myproject.dto.CityDto;
public interface CityService {
	public void save(CityDto cityDto);
	public List<CityDto> getAll();
	public	CityDto findById(Long id);
}