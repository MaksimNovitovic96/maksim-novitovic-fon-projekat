package maksim.novitovic.fon.myproject.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import maksim.novitovic.fon.myproject.dto.CityDto;
import maksim.novitovic.fon.myproject.entity.CityEntity;

@Component
public class CityConverter {

	public CityDto entityToDto(CityEntity cityEntity) {
		if(cityEntity==null) return null;
		return new CityDto(cityEntity.getId(), cityEntity.getNumber(), cityEntity.getName());
	}

	public CityEntity dtoToEntity(CityDto cityDto) {
		if(cityDto==null) return null;
		return new CityEntity(cityDto.getId(), cityDto.getNumber(), cityDto.getName());
	}
	public List<CityEntity> convertDtoListToEntityList(List<CityDto> dtoList) {
		List<CityEntity> cityEntityList = new ArrayList<CityEntity>();
		for(CityDto cityDto: dtoList) {
			cityEntityList.add(dtoToEntity(cityDto));
		}
		return cityEntityList;
	}
	public List<CityDto> convertEntityListToDtoList(List<CityEntity> entityList) {
		List<CityDto> cityDtoList = new ArrayList<CityDto>();
		for(CityEntity city: entityList) {
			cityDtoList.add(entityToDto(city));
		}
		return cityDtoList;
	}
}
