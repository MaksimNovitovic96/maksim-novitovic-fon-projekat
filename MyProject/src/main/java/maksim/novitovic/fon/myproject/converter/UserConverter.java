package maksim.novitovic.fon.myproject.converter;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Component;

import maksim.novitovic.fon.myproject.dto.UserDto;
import maksim.novitovic.fon.myproject.entity.UserEntity;

@Component
public class UserConverter {
	public UserDto entityToDto(UserEntity user) {
		return new UserDto(user.getId(), user.getUsername(), user.getPassword());
	}

	public UserEntity dtoToEntity(UserDto userDto) {
		return new UserEntity(userDto.getId(), userDto.getUsername(), userDto.getPassword());
	}

	public List<UserDto> convertEntityListToDtoList(List<UserEntity> entityList) {
		List<UserDto> usersDtoList = new ArrayList<UserDto>();
		for (UserEntity user : entityList) {
			usersDtoList.add(entityToDto(user));
		}
		return usersDtoList;
	}

	public List<UserEntity> convertDtoListToEntityList(List<UserDto> dtoList) {
		List<UserEntity> userEntityList = new ArrayList<UserEntity>();
		for (UserDto userDto : dtoList) {
			userEntityList.add(dtoToEntity(userDto));
		}
		return userEntityList;
	}
}
