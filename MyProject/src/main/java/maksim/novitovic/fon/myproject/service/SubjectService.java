package maksim.novitovic.fon.myproject.service;

import java.util.List;

import maksim.novitovic.fon.myproject.dto.SubjectDto;

public interface SubjectService {

	public void save(SubjectDto subjectDto);
//	public void update(Long id,SubjectDto subjectDto);
	public void deleteById(Long id);
	public SubjectDto findById(Long id);
	public List<SubjectDto> getAll();
}
