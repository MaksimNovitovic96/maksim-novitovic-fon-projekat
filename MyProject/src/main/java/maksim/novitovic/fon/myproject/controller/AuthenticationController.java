package maksim.novitovic.fon.myproject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import maksim.novitovic.fon.myproject.dto.UserDto;
import maksim.novitovic.fon.myproject.service.UserService;

@Controller
@RequestMapping(value="authentication")
public class AuthenticationController {
	private final UserService userService;
	
	
	@Autowired
	public AuthenticationController(UserService userService) {
		this.userService = userService;
	}

	@GetMapping(value = "login")
	public ModelAndView login() {
		ModelAndView modelAndView = new ModelAndView("authentication/login");
		modelAndView.addObject("userDto", new UserDto());
		return modelAndView;
	}

	@PostMapping(value = "login")
	public ModelAndView authenticate(@ModelAttribute("userDto") UserDto userDto, BindingResult result) {
		System.out.println("===============================================================");
		System.out.println("AuthenticationController: authenticate()=======================");
		System.out.println("===============================================================");
		System.out.println(userDto);
		ModelAndView modelAndView = new ModelAndView();
		
		if(result.hasErrors()) {
			modelAndView.setViewName("/authentication/login");
			
			modelAndView.addObject("userDto", userDto);
		} else {
			modelAndView.addObject("user", userDto);
			
			modelAndView.setViewName("index");
		}
		return modelAndView;
	}

	@ModelAttribute(name = "userDto")
	public UserDto generateUserDto() {
		return new UserDto();
	}
}
