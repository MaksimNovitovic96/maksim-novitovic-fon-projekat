package maksim.novitovic.fon.myproject.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import maksim.novitovic.fon.myproject.dto.StudentDto;
import maksim.novitovic.fon.myproject.entity.StudentEntity;
@Component
public class StudentConverter {
	CityConverter cityConverter;

	@Autowired
	public StudentConverter(CityConverter cityConverter) {
		this.cityConverter = cityConverter;
	}

	public StudentDto entityToDto(StudentEntity studentEntity) {
		if(studentEntity==null) return null;
		return new StudentDto(studentEntity.getId(), studentEntity.getIndexNumber(), studentEntity.getFirstName(),
				studentEntity.getLastName(), studentEntity.getEmail(), studentEntity.getAddress(),
				studentEntity.getPhone(), studentEntity.getCurrentYearOfStudy(),
				cityConverter.entityToDto(studentEntity.getCity()));
	}

	public StudentEntity dtoToEntity(StudentDto studentDto) {
		if(studentDto==null) return null;
		return new StudentEntity(studentDto.getId(), studentDto.getIndexNumber(), studentDto.getFirstName(),
				studentDto.getLastName(), studentDto.getEmail(), studentDto.getAddress(), studentDto.getPhone(),
				studentDto.getCurrentYearOfStudy(), cityConverter.dtoToEntity(studentDto.getCityDto()));
	}
}
