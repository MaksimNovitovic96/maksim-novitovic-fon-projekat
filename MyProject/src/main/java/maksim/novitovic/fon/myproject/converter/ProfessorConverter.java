package maksim.novitovic.fon.myproject.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import maksim.novitovic.fon.myproject.dto.ProfessorDto;
import maksim.novitovic.fon.myproject.entity.ProfessorEntity;

@Component
public class ProfessorConverter {
	CityConverter cityConverter;
	TitleConverter titleConverter;

	@Autowired
	public ProfessorConverter(CityConverter cityConverter, TitleConverter titleConverter) {
		this.cityConverter = cityConverter;
		this.titleConverter = titleConverter;
	}

	public ProfessorDto entityToDto(ProfessorEntity professor) {
		if(professor==null) return null;
		return new ProfessorDto(professor.getId(), professor.getFirstName(), professor.getFirstName(),
				professor.getEmail(), professor.getAddress(), professor.getPhone(), professor.getReelectionDate(),
				cityConverter.entityToDto(professor.getCity()), titleConverter.entityToDto(professor.getTitle()));
	}

	public ProfessorEntity dtoToEntity(ProfessorDto professorDto) {
		if(professorDto==null) return null;
		return new ProfessorEntity(professorDto.getId(), professorDto.getFirstname(), professorDto.getLastname(),
				professorDto.getEmail(), professorDto.getAddress(), professorDto.getPhone(),
				professorDto.getReelectionDate(), cityConverter.dtoToEntity(professorDto.getCityDto()),
				titleConverter.dtoToEntity(professorDto.getTitleDto()));
	}

	public List<ProfessorEntity> convertDtoListToEntityList(List<ProfessorDto> dtoList) {
		List<ProfessorEntity> professorEntityList = new ArrayList<ProfessorEntity>();
		for (ProfessorDto professorDto : dtoList) {
			professorEntityList.add(dtoToEntity(professorDto));
		}
		return professorEntityList;
	}

	public List<ProfessorDto> convertEntityListToDtoList(List<ProfessorEntity> entityList) {
		List<ProfessorDto> professorDtoList = new ArrayList<ProfessorDto>();
		for (ProfessorEntity professor : entityList) {
			professorDtoList.add(entityToDto(professor));
		}
		return professorDtoList;
	}

}