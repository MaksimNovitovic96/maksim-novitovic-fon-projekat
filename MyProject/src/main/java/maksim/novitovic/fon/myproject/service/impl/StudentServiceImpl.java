package maksim.novitovic.fon.myproject.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import maksim.novitovic.fon.myproject.converter.CityConverter;
import maksim.novitovic.fon.myproject.converter.StudentConverter;
import maksim.novitovic.fon.myproject.dto.StudentDto;
import maksim.novitovic.fon.myproject.entity.StudentEntity;
import maksim.novitovic.fon.myproject.repository.jpa.StudentRepository;
import maksim.novitovic.fon.myproject.service.StudentService;

@Service
@Transactional
public class StudentServiceImpl implements StudentService {
	private StudentRepository studentRepository;
	private StudentConverter studentConverter;
	private CityConverter cityConverter;

	@Autowired
	public StudentServiceImpl(StudentRepository studentRepository, StudentConverter studentConverter,
			CityConverter cityConverter) {
		super();
		this.studentRepository = studentRepository;
		this.studentConverter = studentConverter;
		this.cityConverter = cityConverter;
	}

	@Override
	public void save(StudentDto studentDto) {
		studentRepository.save(studentConverter.dtoToEntity(studentDto));

	}

	public void update(Long id, StudentDto studentDto) {
		StudentEntity updatedStudent = studentRepository.findById(id).get();
		updatedStudent.setIndexNumber(studentDto.getIndexNumber());
		updatedStudent.setFirstName(studentDto.getFirstName());
		updatedStudent.setLastName(studentDto.getLastName());
		updatedStudent.setEmail(studentDto.getEmail());
		updatedStudent.setAddress(studentDto.getAddress());
		updatedStudent.setPhone(studentDto.getPhone());
		updatedStudent.setCity(cityConverter.dtoToEntity(studentDto.getCityDto()));
		studentRepository.save(updatedStudent);
	}

	@Override
	public void delete(Long id) {
		studentRepository.deleteById(id);

	}

	@Override
	public StudentDto findById(Long id) {
		return studentConverter.entityToDto(studentRepository.findById(id).get());
	}

	@Override
	public List<StudentDto> getAll() {
		List<StudentEntity> students = studentRepository.findAll();
		List<StudentDto> studentsDto = new ArrayList<StudentDto>();
		for (StudentEntity student : students) {
			studentsDto.add(studentConverter.entityToDto(student));
		}
		return studentsDto;

	}

}
