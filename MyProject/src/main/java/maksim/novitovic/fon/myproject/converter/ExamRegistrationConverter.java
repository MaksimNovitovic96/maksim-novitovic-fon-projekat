package maksim.novitovic.fon.myproject.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import maksim.novitovic.fon.myproject.dto.ExamRegistrationDto;
import maksim.novitovic.fon.myproject.entity.ExamRegistrationEntity;

@Component
public class ExamRegistrationConverter {
	ExamConverter examConverter;
	StudentConverter studentConverter;

	@Autowired
	public ExamRegistrationConverter(ExamConverter examConverter, StudentConverter studentConverter) {
		this.examConverter = examConverter;
		this.studentConverter = studentConverter;
	}

	public ExamRegistrationDto entityToDto(ExamRegistrationEntity examRegistrationEntity) {
		if(examRegistrationEntity==null) return null;
		return new ExamRegistrationDto(examRegistrationEntity.getId(),
				examConverter.entityToDto(examRegistrationEntity.getExam()),
				studentConverter.entityToDto(examRegistrationEntity.getStudent()));
	}

	public ExamRegistrationEntity dtoToEntity(ExamRegistrationDto examRegistrationDto) {
		if(examRegistrationDto==null) return null;
		return new ExamRegistrationEntity(examRegistrationDto.getId(),
				examConverter.dtoToEntity(examRegistrationDto.getExamDto()),
				studentConverter.dtoToEntity(examRegistrationDto.getStudentDto()));
	}
}
