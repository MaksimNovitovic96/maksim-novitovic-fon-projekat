package maksim.novitovic.fon.myproject.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import maksim.novitovic.fon.myproject.dto.CityDto;
import maksim.novitovic.fon.myproject.dto.ProfessorDto;
import maksim.novitovic.fon.myproject.dto.TitleDto;
import maksim.novitovic.fon.myproject.service.CityService;
import maksim.novitovic.fon.myproject.service.ProfessorService;
import maksim.novitovic.fon.myproject.service.TitleService;
import maksim.novitovic.fon.myproject.validator.ProfessorDtoValidator;

@Controller
@RequestMapping(value = "/professor")
public class ProfessorController {

	private final ProfessorService professorService;
	private final CityService cityService;
	private final TitleService titleService;

	@Autowired
	public ProfessorController(ProfessorService professorService, CityService cityService, TitleService titleService) {
		this.professorService = professorService;
		this.cityService = cityService;
		this.titleService = titleService;
	}

	@GetMapping(value = "home")
	public ModelAndView home() {
		System.out.println("====================================================================");
		System.out.println("====================ProfessorController: home()  ===================");
		System.out.println("====================================================================");
		ModelAndView modelAndView = new ModelAndView("professor/home");
		return modelAndView;
	}

	@GetMapping(value = "add")
	public ModelAndView add() {
		System.out.println("====================================================================");
		System.out.println("==================== ProfessorController: add()     ===================");
		System.out.println("====================================================================");
		ModelAndView modelAndView = new ModelAndView("professor/add");
		ProfessorDto professorDto = new ProfessorDto();
		modelAndView.addObject("professorDto", professorDto);
		return modelAndView;
	}

	@PostMapping(value = "save")
	public ModelAndView save(@Valid @ModelAttribute(name = "professorDto") ProfessorDto professorDto,
			BindingResult result) {
		System.out.println("================================  =================================");
		System.out.println(professorDto);
		System.out.println("================================  =================================");

		ModelAndView modelAndView = new ModelAndView();
		if (result.hasErrors()) {
			System.out.println("================================ NOT OK =================================");
			modelAndView.setViewName("professor/add");
			modelAndView.addObject("professorDto", professorDto);
		} else {
			System.out.println("================================     OK =================================");
			modelAndView.setViewName("professor/all");
			professorService.save(professorDto);
		}
		return modelAndView;
	}

	@GetMapping(value = "details")
	public ModelAndView details(@RequestParam("id") Long id) {
		ModelAndView modelAndView = new ModelAndView("professor/details");
		modelAndView.addObject("professorDto", professorService.findById(id));
		return modelAndView;
	}

	@GetMapping(value = "edit")
	public ModelAndView edit(@RequestParam("id") Long id) {
		ModelAndView modelAndView = new ModelAndView("professor/edit");
		modelAndView.addObject("professorDto", professorService.findById(id));
		return modelAndView;
	}

	@PostMapping(value = "update")
	public ModelAndView update(@Valid @ModelAttribute("professorDto") ProfessorDto professorDto, BindingResult result) {
		ModelAndView modelAndView = new ModelAndView();
		if (result.hasErrors()) {
			modelAndView.setViewName("professor/edit");
			modelAndView.addObject("professorDto", professorDto);
		} else {
			professorService.save(professorDto);
			modelAndView.setViewName("professor/all");
		}
		return modelAndView;
	}

	@GetMapping(value = "remove")
	public ModelAndView remove(@RequestParam("id") Long id) {
		ModelAndView modelAndView = new ModelAndView("professor/all");
		professorService.delete(id);
		return modelAndView;
	}

	@GetMapping(value = "all")
	public ModelAndView all() {
		ModelAndView modelAndView = new ModelAndView("professor/all");
		return modelAndView;
	}

	@ModelAttribute(value = "cities")
	public List<CityDto> cities() {
		return cityService.getAll();
	}

	@ModelAttribute(value = "professorDto")
	public ProfessorDto professorDto() {
		ProfessorDto professorDto = new ProfessorDto();
		return professorDto;
	}

	@ModelAttribute(value = "professors")
	public List<ProfessorDto> professorsDto() {
		return professorService.getAll();
	}

	@ModelAttribute(name = "titles")
	public List<TitleDto> titleDto() {
		return titleService.getAll();
	}

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.addValidators(new ProfessorDtoValidator());
	}

}
