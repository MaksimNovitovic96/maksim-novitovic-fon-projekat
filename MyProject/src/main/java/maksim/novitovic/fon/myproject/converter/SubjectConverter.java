package maksim.novitovic.fon.myproject.converter;

import org.springframework.stereotype.Component;

import maksim.novitovic.fon.myproject.dto.SubjectDto;
import maksim.novitovic.fon.myproject.entity.SubjectEntity;

@Component
public class SubjectConverter {

	public SubjectDto entityToDto(SubjectEntity subjectEntity) {
		if(subjectEntity==null) return null;
		return new SubjectDto(subjectEntity.getId(), subjectEntity.getName(), subjectEntity.getDescription(),
				subjectEntity.getYearOfStudy(), subjectEntity.getSemester());
	}

	public SubjectEntity dtoToEntity(SubjectDto subjectDto) {
		if(subjectDto==null) return null;
		return new SubjectEntity(subjectDto.getId(), subjectDto.getName(), subjectDto.getDescription(),
				subjectDto.getYearOfStudy(), subjectDto.getSemester());
	}
}
