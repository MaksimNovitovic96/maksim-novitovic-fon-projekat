package maksim.novitovic.fon.myproject.repository.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Repository;
import maksim.novitovic.fon.myproject.entity.StudentEntity;


@Repository
public interface StudentRepository extends JpaRepository<StudentEntity, Long>{

	StudentEntity findByIndexNumber(String indexNumber);
	
}
