package maksim.novitovic.fon.myproject.dto;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

public class ProfessorDto implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long id;

	@NotEmpty(message = "You must add firstname.")
	@Length(min = 3, message = "Minimum number of characters for Firstname is 3")
	private String firstname;

	@NotEmpty(message = "You must add lastname.")
	@Length(min = 3, message = "Minimum number of characters for Lastname is 3")
	private String lastname;

	@NotEmpty(message = "You must add email.")
	private String email;

	@NotEmpty(message = "You must add address.")
	private String address;

	@NotEmpty(message = "You must add phone number.")
	private String phone;

	@NotNull(message = "You must add reelectionDate")
	private Date reelectionDate;

	private CityDto cityDto;

	private TitleDto titleDto;
public ProfessorDto() {
	// TODO Auto-generated constructor stub
}




	public ProfessorDto(Long id,
		@NotEmpty(message = "You must add firstname.") @Length(min = 3, message = "Minimum number of characters for Firstname is 3") String firstname,
		@NotEmpty(message = "You must add lastname.") @Length(min = 3, message = "Minimum number of characters for Lastname is 3") String lastname,
		@NotEmpty(message = "You must add email.") String email,
		@NotEmpty(message = "You must add address.") String address,
		@NotEmpty(message = "You must add phone number.") String phone,
		@NotNull(message = "You must add reelectionDate") Date reelectionDate, CityDto cityDto, TitleDto titleDto) {
	super();
	this.id = id;
	this.firstname = firstname;
	this.lastname = lastname;
	this.email = email;
	this.address = address;
	this.phone = phone;
	this.reelectionDate = reelectionDate;
	this.cityDto = cityDto;
	this.titleDto = titleDto;
}




	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Date getReelectionDate() {
		return reelectionDate;
	}

	public void setReelectionDate(Date reelectionDate) {
		this.reelectionDate = reelectionDate;
	}

	public CityDto getCityDto() {
		return cityDto;
	}

	public void setCityDto(CityDto cityDto) {
		this.cityDto = cityDto;
	}

	public TitleDto getTitleDto() {
		return titleDto;
	}

	public void setTitleDto(TitleDto titleDto) {
		this.titleDto = titleDto;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + ((cityDto == null) ? 0 : cityDto.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((firstname == null) ? 0 : firstname.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((lastname == null) ? 0 : lastname.hashCode());
		result = prime * result + ((phone == null) ? 0 : phone.hashCode());
		result = prime * result + ((reelectionDate == null) ? 0 : reelectionDate.hashCode());
		result = prime * result + ((titleDto == null) ? 0 : titleDto.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProfessorDto other = (ProfessorDto) obj;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (cityDto == null) {
			if (other.cityDto != null)
				return false;
		} else if (!cityDto.equals(other.cityDto))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (firstname == null) {
			if (other.firstname != null)
				return false;
		} else if (!firstname.equals(other.firstname))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (lastname == null) {
			if (other.lastname != null)
				return false;
		} else if (!lastname.equals(other.lastname))
			return false;
		if (phone == null) {
			if (other.phone != null)
				return false;
		} else if (!phone.equals(other.phone))
			return false;
		if (reelectionDate == null) {
			if (other.reelectionDate != null)
				return false;
		} else if (!reelectionDate.equals(other.reelectionDate))
			return false;
		if (titleDto == null) {
			if (other.titleDto != null)
				return false;
		} else if (!titleDto.equals(other.titleDto))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ProfessorDto [id=" + id + ", firstname=" + firstname + ", lastname=" + lastname + ", email=" + email
				+ ", address=" + address + ", phone=" + phone + ", reelectionDate=" + reelectionDate + ", cityDto="
				+ cityDto + ", titleDto=" + titleDto + "]";
	}
	public String getFullname() {
		return firstname + " " + lastname;
	}
}