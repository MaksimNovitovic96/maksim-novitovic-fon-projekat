package maksim.novitovic.fon.myproject.formatter;

import java.text.ParseException;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;

import maksim.novitovic.fon.myproject.dto.TitleDto;
import maksim.novitovic.fon.myproject.service.TitleService;

public class TitleDtoFormatter implements Formatter<TitleDto> {
	private final TitleService titleService;

	@Autowired
	public TitleDtoFormatter(TitleService titleService) {
		this.titleService = titleService;
	}

	@Override
	public String print(TitleDto titleDto, Locale locale) {

		return titleDto.getId().toString();
	}

	@Override
	public TitleDto parse(String text, Locale locale) throws ParseException {
		Long id = Long.parseLong(text);
		TitleDto titleDto = titleService.findById(id);
		return titleDto;
	}

}