package maksim.novitovic.fon.myproject.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import maksim.novitovic.fon.myproject.converter.ExamConverter;
import maksim.novitovic.fon.myproject.dto.ExamDto;
import maksim.novitovic.fon.myproject.entity.ExamEntity;
import maksim.novitovic.fon.myproject.repository.jpa.ExamRepository;
import maksim.novitovic.fon.myproject.service.ExamService;

@Service
@Transactional
public class ExamServiceImpl implements ExamService {

	private ExamRepository examRepository;
	private ExamConverter examConverter;

	@Autowired
	public ExamServiceImpl(ExamRepository examRepository, ExamConverter examConverter) {
		super();
		this.examRepository = examRepository;
		this.examConverter = examConverter;

	}

	@Override
	public void save(ExamDto examDto) {
		examRepository.save(examConverter.dtoToEntity(examDto));

	}

	@Override
	public ExamDto findById(Long id) {
		return examConverter.entityToDto(examRepository.findById(id).get());
	}

	@Override
	public List<ExamDto> getAll() {
		List<ExamEntity> exams = examRepository.findAll();
		List<ExamDto> examsDto = new ArrayList<ExamDto>();
		for (ExamEntity exam : exams) {
			examsDto.add(examConverter.entityToDto(exam));
		}
		return examsDto;
	}

}
