package maksim.novitovic.fon.myproject.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import maksim.novitovic.fon.myproject.converter.SubjectConverter;
import maksim.novitovic.fon.myproject.dto.SubjectDto;
import maksim.novitovic.fon.myproject.entity.SubjectEntity;
import maksim.novitovic.fon.myproject.repository.jpa.SubjectRepository;
import maksim.novitovic.fon.myproject.service.SubjectService;

@Service
@Transactional
public class SubjectServiceImpl implements SubjectService {

	private SubjectRepository subjectRepository;
	private SubjectConverter subjectConverter;

	@Autowired
	public SubjectServiceImpl(SubjectRepository subjectRepository,
			maksim.novitovic.fon.myproject.converter.SubjectConverter subjectConverter) {
		super();
		this.subjectRepository = subjectRepository;
		this.subjectConverter = subjectConverter;
	}

	public void save(SubjectDto subjectDto) {
		subjectRepository.save(subjectConverter.dtoToEntity(subjectDto));

	}

	public void update(Long id, SubjectDto subjectDto) {
		SubjectEntity updatedSubject = subjectRepository.findById(id).get();
		updatedSubject.setName(subjectDto.getName());
		updatedSubject.setDescription(subjectDto.getDescription());
		updatedSubject.setYearOfStudy(subjectDto.getYearOfStudy());
		updatedSubject.setSemester(subjectDto.getSemester());
		subjectRepository.save(updatedSubject);
	}

	public void deleteById(Long id) {
		subjectRepository.deleteById(id);

	}

	public SubjectDto findById(Long id) {
		return subjectConverter.entityToDto(subjectRepository.findById(id).get());
	}

	public List<SubjectDto> getAll() {
		List<SubjectEntity> subjects = subjectRepository.findAll();
		List<SubjectDto> subjectsDto = new ArrayList<SubjectDto>();
		for (SubjectEntity subject : subjects) {
			subjectsDto.add(subjectConverter.entityToDto(subject));
		}
		return subjectsDto;
	}
}
