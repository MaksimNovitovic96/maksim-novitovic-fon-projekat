package maksim.novitovic.fon.myproject.service.impl;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import maksim.novitovic.fon.myproject.converter.CityConverter;
import maksim.novitovic.fon.myproject.converter.ProfessorConverter;
import maksim.novitovic.fon.myproject.converter.TitleConverter;
import maksim.novitovic.fon.myproject.dto.ProfessorDto;
import maksim.novitovic.fon.myproject.entity.ProfessorEntity;
import maksim.novitovic.fon.myproject.repository.jpa.ProfessorRepository;
import maksim.novitovic.fon.myproject.service.ProfessorService;

@Service
@Transactional
public class ProfessorServiceImpl implements ProfessorService{
	
	
	private ProfessorRepository professorRepository;
	private ProfessorConverter professorConverter;

	@Autowired
	public ProfessorServiceImpl(ProfessorRepository professorRepository, ProfessorConverter professorConverter) {
		this.professorRepository = professorRepository;
		this.professorConverter = professorConverter;
	}

	@Override
	public void save(ProfessorDto professorDto) {
		professorRepository.save(professorConverter.dtoToEntity(professorDto));

	}

	@Override
	public List<ProfessorDto> getAll() {
		List<ProfessorEntity> professors = professorRepository.findAll();
		List<ProfessorDto> dtoProfessors = professorConverter.convertEntityListToDtoList(professors);
		return dtoProfessors;
	}

	@Override
	public ProfessorDto findById(Long id) {
		Optional<ProfessorEntity> professorOpt = professorRepository.findById(id);
		if (professorOpt.isPresent()) {
			return professorConverter.entityToDto(professorOpt.get());
		} else {
			return null;
		}
	}

	@Override
	public void delete(Long id) {
		professorRepository.deleteById(id);

	}

}
