package maksim.novitovic.fon.myproject.config;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;
//public class MyWebApplicationInitializer implements WebApplicationInitializer {
//
//	@Override
//	public void onStartup(ServletContext servletContext) throws ServletException {
//		System.out.println("========================================================");
//		System.out.println("============= MyWebApplicationIznitializer =============");
//		System.out.println("========================================================");
//
//		AnnotationConfigWebApplicationContext webApplicationContext = new AnnotationConfigWebApplicationContext();
//		webApplicationContext.register(MyWebContextConfig.class);
//		webApplicationContext.setServletContext(servletContext);
//
//		ServletRegistration.Dynamic dispatcherServlet = servletContext.addServlet("myDispatcherServlet",
//				new DispatcherServlet(webApplicationContext));
//		dispatcherServlet.addMapping("/");
//		dispatcherServlet.setLoadOnStartup(1);
//	}
//}


public class MyWebApplicationInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

	@Override
	protected Class<?>[] getRootConfigClasses() {
		return new Class[] {MyDatabaseConfig.class};
	}

	@Override
	protected Class<?>[] getServletConfigClasses() {
		return new Class[] { MyWebContextConfig.class};
	}

	@Override
	protected String[] getServletMappings() {
		return new String[] { "/" };
	}
}
