package maksim.novitovic.fon.myproject.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import maksim.novitovic.fon.myproject.converter.UserConverter;
import maksim.novitovic.fon.myproject.dto.UserDto;
import maksim.novitovic.fon.myproject.entity.UserEntity;
import maksim.novitovic.fon.myproject.repository.jpa.UserRepository;
import maksim.novitovic.fon.myproject.service.UserService;

@Service
public class UserServiceImpl implements UserService {
	private final UserRepository userRepository;
	private final UserConverter userConverter;
	
	
	@Autowired
	public UserServiceImpl(UserRepository userRepository, UserConverter userConverter) {
		this.userRepository = userRepository;
		this.userConverter = userConverter;
	}


	@Override
	public List<UserDto> findUserByNameAndPassword(UserDto userDto) {
		List<UserEntity> users = userRepository.findByUsernameAndPassword(userDto.getUsername(), userDto.getPassword());
		List<UserDto> listUserDto = userConverter.convertEntityListToDtoList(users);
		return listUserDto;
	}
}
