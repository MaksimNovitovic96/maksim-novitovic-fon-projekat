package maksim.novitovic.fon.myproject.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import maksim.novitovic.fon.myproject.dto.ExamDto;
import maksim.novitovic.fon.myproject.entity.ExamEntity;

@Component
public class ExamConverter {

	ProfessorConverter professorConverter;
	SubjectConverter subjectConverter;

	@Autowired
	public ExamConverter(ProfessorConverter professorConverter, SubjectConverter subjectConverter) {
		this.professorConverter = professorConverter;
		this.subjectConverter = subjectConverter;
	}

	public ExamDto entityToDto(ExamEntity examEntity) {
		if(examEntity==null) return null;
		return new ExamDto(examEntity.getId(), examEntity.getDate(),
				professorConverter.entityToDto(examEntity.getProfessor()),
				subjectConverter.entityToDto(examEntity.getSubject()));
	}

	public ExamEntity dtoToEntity(ExamDto examDto) {
		if(examDto==null) return null;
		return new ExamEntity(examDto.getId(), examDto.getDate(),
				professorConverter.dtoToEntity(examDto.getProfessorDto()),
				subjectConverter.dtoToEntity(examDto.getSubjectDto()));
	}
}
