package maksim.novitovic.fon.myproject.service;

import java.util.List;

import maksim.novitovic.fon.myproject.dto.ExamRegistrationDto;

public interface ExamRegistrationService {

	public void save(ExamRegistrationDto examRegistrationDto);
	public ExamRegistrationDto findById(Long id);
	public List<ExamRegistrationDto> getAll();
}
