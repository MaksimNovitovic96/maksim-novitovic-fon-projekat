package maksim.novitovic.fon.myproject.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import maksim.novitovic.fon.myproject.converter.TitleConverter;
import maksim.novitovic.fon.myproject.dto.TitleDto;
import maksim.novitovic.fon.myproject.entity.TitleEntity;
import maksim.novitovic.fon.myproject.repository.jpa.TitleRepository;
import maksim.novitovic.fon.myproject.service.TitleService;

@Service
@Transactional
public class TitleServiceImpl implements TitleService {

	TitleRepository titleRepository;
	TitleConverter titleConverter;

	@Autowired
	public TitleServiceImpl(TitleRepository titleRepository, TitleConverter titleConverter) {
		super();
		this.titleRepository = titleRepository;
		this.titleConverter = titleConverter;
	}

	@Override
	public TitleDto findById(Long id) {
		return titleConverter.entityToDto(titleRepository.findById(id).get());
	}

	@Override
	public List<TitleDto> getAll() {

		List<TitleEntity> titles = titleRepository.findAll();
		List<TitleDto> titlesDto = new ArrayList<TitleDto>();
		for (TitleEntity title : titles) {
			titlesDto.add(titleConverter.entityToDto(title));
		}
		return titlesDto;
	}

	@Override
	public void save(TitleDto titleDto) {
		titleRepository.save(titleConverter.dtoToEntity(titleDto));
		
	}

}
