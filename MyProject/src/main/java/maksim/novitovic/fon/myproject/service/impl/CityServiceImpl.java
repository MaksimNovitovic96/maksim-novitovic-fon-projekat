package maksim.novitovic.fon.myproject.service.impl;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import maksim.novitovic.fon.myproject.converter.CityConverter;
import maksim.novitovic.fon.myproject.dto.CityDto;
import maksim.novitovic.fon.myproject.entity.CityEntity;
import maksim.novitovic.fon.myproject.repository.jpa.CityRepository;
import maksim.novitovic.fon.myproject.service.CityService;

@Service
@Transactional
public class CityServiceImpl implements CityService {

	private CityRepository cityRepository;
	private CityConverter cityConverter;

	@Autowired
	public CityServiceImpl(CityRepository cityRepository, CityConverter cityConverter) {
		this.cityRepository = cityRepository;
		this.cityConverter = cityConverter;
	}

	@Override
	public void save(CityDto cityDto) {
		cityRepository.save(cityConverter.dtoToEntity(cityDto));

	}

	@Override
	public List<CityDto> getAll() {
		List<CityEntity> cities = cityRepository.findAll();
		List<CityDto> listCitiesDto = cityConverter.convertEntityListToDtoList(cities);
		return listCitiesDto;
	}

	@Override
	public CityDto findById(Long id) {
		Optional<CityEntity> optionalCity = cityRepository.findById(id);
		if (optionalCity.isPresent()) {
			return cityConverter.entityToDto(optionalCity.get());
		} else {
			return null;
		}
	}

}
