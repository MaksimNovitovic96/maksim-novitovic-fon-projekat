package maksim.novitovic.fon.myproject.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import maksim.novitovic.fon.myproject.dto.StudentDto;

public class StudentDtoValidator implements Validator {
	@Override
	public boolean supports(Class<?> clazz) {
		
		return StudentDto.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		StudentDto studentDto = (StudentDto) target;
		
		if(studentDto.getEmail()!=null) {
			if(!studentDto.getEmail().contains("@")) {
				errors.rejectValue("email", "StudentDto.email", "Email must contains @");
			}
		}
		
		if(studentDto.getPhone()!=null) {
			if(studentDto.getPhone().length()<6) {
				errors.rejectValue("phone", "StudentDto.phone", "Minimal number for phone is 6");
			}
		}
		if(studentDto.getAddress()!=null) {
			if(studentDto.getAddress().length()<3) {
				errors.rejectValue("address", "StudentDto.address", "Minimal number characters for address is 3");
			}
		}
		
	}
}
