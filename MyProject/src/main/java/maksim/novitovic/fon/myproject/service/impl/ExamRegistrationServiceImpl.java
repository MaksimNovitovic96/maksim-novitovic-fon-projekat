package maksim.novitovic.fon.myproject.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import maksim.novitovic.fon.myproject.converter.ExamRegistrationConverter;
import maksim.novitovic.fon.myproject.dto.ExamRegistrationDto;
import maksim.novitovic.fon.myproject.entity.ExamRegistrationEntity;
import maksim.novitovic.fon.myproject.repository.jpa.ExamRegistrationRepository;
import maksim.novitovic.fon.myproject.service.ExamRegistrationService;

@Service
@Transactional
public class ExamRegistrationServiceImpl implements ExamRegistrationService {

	ExamRegistrationRepository examRegistrationRepository;
	ExamRegistrationConverter examRegistrationConverter;

	public ExamRegistrationServiceImpl(ExamRegistrationRepository examRegistrationRepository,
			ExamRegistrationConverter examRegistrationConverter) {
		super();
		this.examRegistrationRepository = examRegistrationRepository;
		this.examRegistrationConverter = examRegistrationConverter;
	}

	@Override
	public void save(ExamRegistrationDto examRegistrationDto) {
		examRegistrationRepository.save(examRegistrationConverter.dtoToEntity(examRegistrationDto));

	}

	@Override
	public ExamRegistrationDto findById(Long id) {
		return examRegistrationConverter.entityToDto(examRegistrationRepository.findById(id).get());
	}

	@Override
	public List<ExamRegistrationDto> getAll() {
		List<ExamRegistrationEntity> examRegistrations = examRegistrationRepository.findAll();
		List<ExamRegistrationDto> examRegistrationsDto = new ArrayList<ExamRegistrationDto>();
		for (ExamRegistrationEntity examRegistration : examRegistrations) {
			examRegistrationsDto.add(examRegistrationConverter.entityToDto(examRegistration));
		}
		return examRegistrationsDto;
	}

}
