package maksim.novitovic.fon.myproject.repository.jpa;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import maksim.novitovic.fon.myproject.entity.CityEntity;
import maksim.novitovic.fon.myproject.entity.StudentEntity;


@Repository
public interface CityRepository extends JpaRepository<CityEntity, Long>{

	
}
