package maksim.novitovic.fon.myproject.repository.jpa;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import maksim.novitovic.fon.myproject.entity.UserEntity;

@Repository
public interface UserRepository extends JpaRepository<UserEntity,Long> {
	public List<UserEntity> findByUsernameAndPassword(String username, String password);

}
